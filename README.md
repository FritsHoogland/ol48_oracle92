This is a project to install oracle 9.2 on an Oracle Enterprise Linux 4.8 server.

The use of this software is at your own risk, and you need to fulfil the licensing requirements of the products used.
This software is legacy and out of support, this is created for demo and historic purposes.

This software is an aid to install oracle and create a database unattended.
Because this OS is so old, it can't run Ansible in a simple way, and therefore it's scripted in bash.
This means the scripts can not (nicely) be rerun (they are not idempotent). If it breaks, fix it, clean up the VM and try again.

Currently, it can install version 9.2.0.4, 9.2.0.5, 9.2.0.6, 9.2.0.7 and 9.2.0.8.

## a) the environment
This project requires the following local installs:
- virtualbox
- vagrant
- git

## b) obtain the vagrant project and the scripts:
`git clone https://gitlab.com/FritsHoogland/ol48_oracle92.git o92`

## c) edit the o92/Vagrantfile file
By default, the database_name variable is empty. This means no database is created. If you want a database, set a name for one.
You also might want to change: database_characterset, redologfile_size, pga_aggregate_target_mb, depending on your needs.
The database_version variable determines the version to be installed. The default version is 9.2.0.4, requiring the cpio installation files and a patch, see below with 'add the software'. If you set it to 9.2.0.5, 9.2.0.6, 9.2.0.7 or 9.2.0.8, you must add the patch (zipfile) so it can be used.
Do not change anything else.

## d) add the software
In order to install the oracle database, you must add the oracle installation media to the o92/files directory:
- ship_9204_linux_disk1.cpio
- ship_9204_linux_disk2.cpio
- ship_9204_linux_disk3.cpio
- p4198954_40_LINUX.zip
Patch 4198954 is available if you got a MOS account and download properties.

If you set another version than 9.2.0.4, you must add:
- 9.2.0.5: add: p3501955_9205_LINUX.zip
- 9.2.0.6: add: p3948480_9206_LINUX.zip
- 9.2.0.7: add: p4163445_92070_LINUX.zip
- 9.2.0.8: add: p4547809_92080_LINUX.zip

## e) start vagrant
In order to get the linux image and get the oracle software installed and optionally get the database execute:

`vagrant up`

This will pull the FritsHoogland/oracle-4.8 box, start it, copy the files in the files directory into the vagrant box and do the installation.

## f) other noteworthy things
Currently, the 9.2.0.4 install from the installation source files and the application of patches 9.2.0.7 and 9.2.0.8 does fully work.
Version 9.2.0.5 and 9.2.0.6 throw an error during the linking of the oracle executable. 
For version 9.2.0.5 this means the oracle executable copied from the patch is not relinked. As a result, when you try to create a database it throws ORA-12547: TNS:lost contact errors in the $ORACLE_HOME/assistants/dbca/log/silent.log file.
For version 9.2.0.6 this means the oracle executable copied from the patch is not relinked. Interesting is that this executable still can be used to create and use a database.

This is the 32 bit OS version and the install tested with the 32 bit oracle version.
Oracle Linux 4.8 is not officially supported by vagrant nor virtualbox.
For virtualbox I have not come across issues yet.
For vagrant, this means some actions from vagrant to administer the VM throw an error. These are setting hostname (so I removed that), adding a network interface (so I removed that) and shutting down the VM using vagrant halt. Use 'sudo /sbin/shutdown -h now' instead.
Currently, the stop/start script doesn't work. So start the database by hand.
The vagrant box FritsHoogland/oracle-4.8 is build using packer, here is the build project: https://gitlab.com/FritsHoogland/packer-oracle-linux-museum

If you see anything that might improve this, contact me at frits.hoogland_at_gmail.com
(replace _at_ with '@')