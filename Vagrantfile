# -*- mode: ruby -*-
# vi: set ft=ruby :

######################################################################
# Customisation. Change variables here!
#
database_name = ""
#
global_password = "oracle"
#
stage_directory = "/u01/install"
#
linux = "FritsHoogland/oracle-4.8"
#
vm_cpus = "2"
vm_memory = "2048"
#
database_characterset = "WE8ISO8859P1"
redologfile_size = "100"
sga_target_mb = "2000"
pga_aggregate_target_mb = "500"
#
database_version = "9.2.0.4"
#database_version = "9.2.0.5" -- warning: throws error during linking the oracle executable, database creation throws ORA-12547: TNS:lost contact and doesn't finish.
#database_version = "9.2.0.6" -- warning: throws error during linking the oracle executable, but database creation possible.
#database_version = "9.2.0.7"
#database_version = "9.2.0.8"
#
######################################################################
Vagrant.configure("2") do |config|
  config.vm.box = "#{linux}"
  config.ssh.insert_key = false
  config.ssh.sudo_command = "sudo -H %c"
  config.vm.hostname = nil
  config.vm.synced_folder ".", "/vagrant", disabled: true
  config.vm.provider "virtualbox" do |vb|
    vb.memory = "#{vm_memory}"
    vb.cpus = "#{vm_cpus}"
    u01_disk = "u01_disk.vdi"
    if !File.exists?(u01_disk)
      vb.customize [ 'createhd', '--filename', u01_disk, '--size', 40960 ]
    end
    vb.customize [ 'storageattach', :id, '--storagectl', 'SATA Controller', '--port', 2, '--device', 0, '--type', 'hdd', '--medium', u01_disk ]
    data_disk = "data_disk.vdi"
    if !File.exists?(data_disk)
      vb.customize [ 'createhd', '--filename', data_disk, '--size', 40960 ]
    end
    vb.customize [ 'storageattach', :id, '--storagectl', 'SATA Controller', '--port', 3, '--device', 0, '--type', 'hdd', '--medium', data_disk ]
  end
  config.vm.provision "Copy files directory. Can take some time if installation media is present.", type: "file", source: "files", destination: "/home/vagrant/install"
  config.vm.provision "Create settings file.", type: "shell", privileged: false, inline: 'echo "export database_name=${1}
export global_password=${2}
export database_characterset=${3}
export redologfile_size=${4}
export sga_target_mb=${5}
export pga_aggregate_target_mb=${6}
export database_version=${7}
export stage_directory=${8}" > /home/vagrant/install/settings.sh', args: "'#{database_name}' '#{global_password}' '#{database_characterset}' '#{redologfile_size}' '#{sga_target_mb}' '#{pga_aggregate_target_mb}' '#{database_version}' '#{stage_directory}'"
  config.vm.provision "Change all the scripts (.sh) to be executable.", type: "shell", privileged: false, inline: "find /home/vagrant/install -name \"*.sh\" -exec chmod 755 {} \\;"
  config.vm.provision "Run machine and oracle setup tasks.", type: "shell", privileged: false, inline: "/home/vagrant/install/setup.sh"
end
