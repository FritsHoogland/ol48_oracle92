#!/bin/bash
source ./install/settings.sh
./install/scripts/check_files.sh
[ $? -gt 0 ] && exit 1
./install/scripts/setup_u01.sh
./install/scripts/setup_oradata.sh
./install/scripts/install_oracle.sh
[ $database_version = "9.2.0.5" ] && ./install/scripts/patch_9205.sh
[ $database_version = "9.2.0.6" ] && ./install/scripts/patch_9206.sh
[ $database_version = "9.2.0.7" ] && ./install/scripts/patch_9207.sh
[ $database_version = "9.2.0.8" ] && ./install/scripts/patch_9208.sh
./install/scripts/create_database.sh
./install/scripts/setup_autostart.sh
