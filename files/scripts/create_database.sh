#!/bin/bash
#
stage_directory=/u01/install
#
echo "update template and create database"
#
/usr/bin/sudo -u oracle /bin/sed -i "s/__PGA_AGGREGATE_TARGET__/$pga_aggregate_target_mb/" $stage_directory/scripts/database.dbt
/usr/bin/sudo -u oracle /bin/sed -i "s/__REDO_SIZE__/$redologfile_size/" $stage_directory/scripts/database.dbt
/usr/bin/sudo -u oracle /bin/cp $stage_directory/scripts/database.dbt /u01/app/oracle/product/9.2/dbhome_1/assistants/dbca/templates
if [ ! -z "$database_name" ]; then
  /usr/bin/sudo -u oracle sh -c "source /home/oracle/.bash_profile; /u01/app/oracle/product/9.2/dbhome_1/bin/dbca -silent -createdatabase -templatename database.dbt -gdbname $database_name -sid $database_name -characterset $database_characterset"
fi