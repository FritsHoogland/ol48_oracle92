#!/bin/bash
#
/bin/echo "apply patch 9.2.0.7"
#
/usr/bin/sudo /bin/mkdir $stage_directory/patch_9207
/usr/bin/sudo /bin/cp /home/vagrant/install/p4163445_92070_LINUX.zip $stage_directory/patch_9207
/usr/bin/sudo /bin/chown -R oracle.dba $stage_directory
/usr/bin/sudo -u oracle sh -c "cd $stage_directory/patch_9207; /usr/bin/unzip p4163445_92070_LINUX.zip"
/usr/bin/sudo -u oracle /bin/sed -i "s#__PATCH_PRODUCTS_XML__#$stage_directory/patch_9207/Disk1/stage/products.xml#" $stage_directory/scripts/patch_9207.rsp
/usr/bin/sudo -u oracle /bin/sed -i "s#__ORACLE_HOME__#/u01/app/oracle/product/9.2/dbhome_1#" $stage_directory/scripts/patch_9207.rsp
/usr/bin/sudo -u oracle sh -c "export DISPLAY=:99; $stage_directory/patch_9207/Disk1/runInstaller -silent -force -waitforcompletion -responsefile /u01/install/scripts/patch_9207.rsp"
/bin/sleep 10; while [ $(/usr/bin/pgrep java | /usr/bin/wc -l) -gt 0 ]; do /bin/sleep 1; done
/usr/bin/sudo sh -c "export ORACLE_DEFAULT=T; /u01/app/oracle/product/9.2/dbhome_1/root.sh"