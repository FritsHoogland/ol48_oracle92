#!/bin/bash
#
/bin/echo "copy files and install oracle database software"
#
/usr/bin/sudo /bin/mkdir -p $stage_directory/scripts
/usr/bin/sudo /bin/cp /home/vagrant/install/scripts/oracle/* $stage_directory/scripts
/usr/bin/sudo /bin/cp /home/vagrant/install/ship_9204_linux_disk[123].cpio $stage_directory
/usr/bin/sudo /bin/cp /home/vagrant/install/p4198954_40_LINUX.zip $stage_directory
/usr/bin/sudo sh -c "cd $stage_directory; /usr/bin/unzip p4198954_40_LINUX.zip"
/usr/bin/sudo rpm -U $stage_directory/compat-oracle-rhel4-1.0-5.i386.rpm
/usr/bin/sudo rpm -U $stage_directory/compat-libcwait-2.1-1.i386.rpm
/usr/bin/sudo /u01/install/scripts/orainstRoot.sh
/usr/bin/sudo /bin/chown -R oracle.dba $stage_directory
/usr/bin/sudo -u oracle /bin/echo "export ORACLE_BASE=/u01/app/oracle
export ORACLE_HOME=/u01/app/oracle/product/9.2/dbhome_1
export PATH=/usr/sbin:\$ORACLE_HOME/bin:\$PATH
export LD_LIBRARY_PATH=\$ORACLE_HOME/lib:/lib:/usr/lib
export CLASSPATH=\$ORACLE_HOME/JRE:\$ORACLE_HOME/jlib:\$ORACLE_HOME/rdbms/jlib:\$ORACLE_HOME/network/jlib
export LD_ASSUME_KERNEL=2.4.19
export THREADS_FLAG=native
export DISPLAY=:99" | sudo -u oracle /usr/bin/tee -a /home/oracle/.bash_profile > /dev/null
/usr/bin/sudo -u oracle sh -c "cd $stage_directory; /bin/cpio -idm < ship_9204_linux_disk1.cpio"
/usr/bin/sudo -u oracle sh -c "cd $stage_directory; /bin/cpio -idm < ship_9204_linux_disk2.cpio"
/usr/bin/sudo -u oracle sh -c "cd $stage_directory; /bin/cpio -idm < ship_9204_linux_disk3.cpio"
/usr/bin/sudo -u oracle /bin/sed -i "s#__ORACLE_HOME__#/u01/app/oracle/product/9.2/dbhome_1#" $stage_directory/scripts/install.rsp
/usr/bin/sudo -u oracle sh -c "export DISPLAY=:99; $stage_directory/Disk1/runInstaller -silent -force -waitforcompletion -responsefile /u01/install/scripts/install.rsp"
/bin/sleep 10; while [ $(/usr/bin/pgrep java | /usr/bin/wc -l) -gt 0 ]; do /bin/sleep 1; done
/usr/bin/sudo sh -c "export ORACLE_DEFAULT=T; /u01/app/oracle/product/9.2/dbhome_1/root.sh"
