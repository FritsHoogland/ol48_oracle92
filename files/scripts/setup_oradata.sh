#!/bin/bash
#
DATA_DEVICE=/dev/sdc
DATA_SIZE=35G
#
/bin/echo "setup oradata"
#
/usr/bin/sudo /usr/sbin/pvcreate $DATA_DEVICE
/usr/bin/sudo /usr/sbin/vgextend vg_oracle $DATA_DEVICE
/usr/bin/sudo /usr/sbin/lvcreate -n lv_oradata -L $DATA_SIZE vg_oracle
/usr/bin/sudo /sbin/mkfs.ext3 -q /dev/vg_oracle/lv_oradata
/usr/bin/sudo -u oracle /bin/mkdir -p /u01/app/oracle/oradata
/usr/bin/sudo /bin/echo '/dev/vg_oracle/lv_oradata /u01/app/oracle/oradata ext3 defaults 0 0' | sudo tee -a /etc/fstab > /dev/null
/usr/bin/sudo /bin/mount -a
/usr/bin/sudo /bin/chown oracle.dba /u01/app/oracle/oradata
