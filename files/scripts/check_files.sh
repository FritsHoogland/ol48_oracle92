#!/bin/bash
#
/bin/echo "check for necessary files"
#
ERROR_COUNTER=0
#
if [ -f /home/vagrant/install/ship_9204_linux_disk1.cpio ]; then
  /bin/echo "OK - ship_9204_linux_disk1.cpio"  
else
  /bin/echo "FAIL - ship_9204_linux_disk1.cpio"  
  let ERROR_COUNTER++
fi
if [ -f /home/vagrant/install/ship_9204_linux_disk2.cpio ]; then
  /bin/echo "OK - ship_9204_linux_disk2.cpio"  
else
  /bin/echo "FAIL - ship_9204_linux_disk2.cpio"  
  let ERROR_COUNTER++
fi
if [ -f /home/vagrant/install/ship_9204_linux_disk3.cpio ]; then
  /bin/echo "OK - ship_9204_linux_disk3.cpio"  
else
  /bin/echo "FAIL - ship_9204_linux_disk3.cpio"  
  let ERROR_COUNTER++
fi
if [ -f /home/vagrant/install/p4198954_40_LINUX.zip ]; then
  /bin/echo "OK - p4198954_40_LINUX.zip"  
else
  /bin/echo "FAIL - p4198954_40_LINUX.zip"  
  let ERROR_COUNTER++
fi

if [ $database_version = "9.2.0.5" ]; then
  if [ -f /home/vagrant/install/p3501955_9205_LINUX.zip ]; then
    /bin/echo "OK - p3501955_9205_LINUX.zip"
  else
    /bin/echo "FAIL - p3501955_9205_LINUX.zip"
    let ERROR_COUNTER++
  fi
fi

if [ $database_version = "9.2.0.6" ]; then
  if [ -f /home/vagrant/install/p3948480_9206_LINUX.zip ]; then
    /bin/echo "OK - p3948480_9206_LINUX.zip"
  else
    /bin/echo "FAIL - p3948480_9206_LINUX.zip"
    let ERROR_COUNTER++
  fi
fi

if [ $database_version = "9.2.0.7" ]; then
  if [ -f /home/vagrant/install/p4163445_92070_LINUX.zip ]; then
    /bin/echo "OK - p4163445_92070_LINUX.zip"
  else
    /bin/echo "FAIL - p4163445_92070_LINUX.zip"
    let ERROR_COUNTER++
  fi
fi

if [ $database_version = "9.2.0.8" ]; then
  if [ -f /home/vagrant/install/p4547809_92080_LINUX.zip ]; then
    /bin/echo "OK - p4547809_92080_LINUX.zip"
  else
    /bin/echo "FAIL - p4547809_92080_LINUX.zip"
    let ERROR_COUNTER++
  fi
fi

if [ $ERROR_COUNTER -gt 0 ]; then
  /bin/echo "No all files are present. Exiting"
  exit 1
fi