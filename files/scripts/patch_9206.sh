#!/bin/bash
#
/bin/echo "apply patch 9.2.0.6"
#
/usr/bin/sudo /bin/mkdir $stage_directory/patch_9206
/usr/bin/sudo /bin/cp /home/vagrant/install/p3948480_9206_LINUX.zip $stage_directory/patch_9206
/usr/bin/sudo /bin/chown -R oracle.dba $stage_directory
/usr/bin/sudo -u oracle sh -c "cd $stage_directory/patch_9206; /usr/bin/unzip p3948480_9206_LINUX.zip"
/usr/bin/sudo -u oracle /bin/sed -i "s#__PATCH_PRODUCTS_XML__#$stage_directory/patch_9206/Disk1/stage/products.xml#" $stage_directory/scripts/patch_9206.rsp
/usr/bin/sudo -u oracle /bin/sed -i "s#__ORACLE_HOME__#/u01/app/oracle/product/9.2/dbhome_1#" $stage_directory/scripts/patch_9206.rsp
/usr/bin/sudo -u oracle sh -c "export DISPLAY=:99; export LD_ASSUME_KERNEL=2.4.19; $stage_directory/patch_9206/Disk1/runInstaller -silent -force -waitforcompletion -ignoresysprereqs -ignoreprereq -responsefile /u01/install/scripts/patch_9206.rsp"
/bin/sleep 10; while [ $(/usr/bin/pgrep java | /usr/bin/wc -l) -gt 0 ]; do /bin/sleep 1; done
/usr/bin/sudo sh -c "export ORACLE_DEFAULT=T; /u01/app/oracle/product/9.2/dbhome_1/root.sh"