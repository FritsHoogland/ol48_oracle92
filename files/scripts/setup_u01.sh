#!/bin/bash
#
U01_DEVICE=/dev/sdb
U01_SIZE=39G
#
/bin/echo "setup u01"
#
/usr/bin/sudo /usr/sbin/pvcreate $U01_DEVICE
/usr/bin/sudo /usr/sbin/vgcreate vg_oracle $U01_DEVICE
/usr/bin/sudo /usr/sbin/lvcreate -n lv_oracle -L $U01_SIZE vg_oracle
/usr/bin/sudo /sbin/mkfs.ext3 -q /dev/vg_oracle/lv_oracle
/usr/bin/sudo /bin/mkdir /u01
/usr/bin/sudo /bin/echo '/dev/vg_oracle/lv_oracle /u01 ext3 defaults 0 0' | sudo tee -a /etc/fstab > /dev/null
/usr/bin/sudo /bin/mount -a
/usr/bin/sudo /bin/chown oracle.dba /u01
